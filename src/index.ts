// @remove-on-eject-begin
/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of `create-react-app` repository.
 *
 * @see https://github.com/facebook/create-react-app/blob/4562ab6fd80c3e18858b3a9a3828810944c35e36/packages/react-scripts/config/env.js
 */
// @remove-on-eject-end

import fs from "fs"

export = function dotenv() {
  const NODE_ENV = process.env.NODE_ENV

  let dotenvFiles = []

  // https://github.com/bkeepers/dotenv#what-other-env-files-can-i-use
  if (NODE_ENV) {
    dotenvFiles.push(`.env.${NODE_ENV}.local`)
    dotenvFiles.push(`.env.${NODE_ENV}`)

    if (NODE_ENV !== "test") {
      dotenvFiles.push(".env.local")
    }
  }

  dotenvFiles.push(".env")

  dotenvFiles = dotenvFiles.filter(Boolean)

  // Load environment variables from .env* files. Suppress warnings using silent
  // if this file is missing. dotenv will never modify any environment variables
  // that have already been set. Variable expansion is supported in .env files.
  // https://github.com/motdotla/dotenv
  // https://github.com/motdotla/dotenv-expand
  dotenvFiles.forEach((dotenvFile) => {
    if (fs.existsSync(dotenvFile)) {
      require("dotenv-expand")(
        require("dotenv").config({
          path: dotenvFile
        })
      )
    }
  })
}
